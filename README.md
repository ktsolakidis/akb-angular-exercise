# Willkommen bei unserer AKB Angular Aufgabe

Dieses README.md dient der Erklärung, was genau von einem erwartet wird.
Wir wünschen dir dabei viel Spass und viel Erfolg :)

Versionen:

- **Angular**: 13.3.11,
- **NPM**: 7.24.2,
- **Node**: 18.7.0,
- **Bootstrap 5**

Bei Fragen bitte jederzeit bei: [konstantinos.tsolakidis@akb.ch](mailto:konstantinos.tsolakidis@akb.ch) melden.

## Erste Schritte

Am Anfang bitten wir dich, dieses Repository zu klonen und einen eigenen Branch aus main / master zu erstellen.
Am Ende muss keine Pull Request / Change Request erstellt werden. Die Arbeit auf deinem eigenen Branch reicht aus.

Unter `src/app/data` befinden sich 2 Dateien.

- data.ts -> Statische Daten die für die Aufgabe benötigt werden
- model.ts -> Models zu den Daten

**Für die Aufgabe ist es wichtig, dass folgende Punkte eingehalten werden:**

- Bootstrap 5 ( Custom Styling wird nicht benötigt, Ziel ist die Funktionalität und **NICHT** das Aussehen, ein reponsives Verhalten der Applikation ist **NICHT** erwünscht )
- Reactive Forms ( **NICHT** Template Driven Forms )
- Keine Funktionalität die nicht im Abschnitt "Aufgabe" beschrieben wird

## Aufgabe

Bei einer imaginären Bank wird ein Profil eines Kundens zur Verfügung gestellt. In diesem Fall Hans Mustermann.
Es befinden sich in diesem Sample JSON Informationen über seine Person, seine Karten und seine Konten.

0. Erstellen einer Reactive Form.
1. Darstellen der Felder: "firstName", "lastName", "maritalStatus"
   1. Diese Felder dürfen nicht bearbeitbar sein
2. Ein Input field für das Feld "country"
   1. Alle Charaktären sind erlaubt, bis 12 Zeichen darf der User eingeben
   2. Der Endstring muss mit der Variable **VALID_COUNTRIES** verglichen werden und mit einem dieser "name" values übereinstimmen damit die Form valide ist. (Upper / Lower Case darf frei entschieden werden). Falls der Input nicht mit einem dieser Werte übereinstimmt müsste eine Fehlermeldung angezeigt werden und die Form wird invalide. **RXJS** wird erwünscht.
3. In der **CUSTOMER** Variable gibt es ein Property: "cards". Initial hat der Kunde eine Karte. Es wird gewünscht, dass alle Karten angezeigt werden und der Benutzer die Möglichkeit hat, mit einem ( + / - ) neue Karten hinzuzufügen oder existierende zu löschen ( Alle Felder von allen Karten müssen vom Benutzer bearbeitbar sein, beim Hinzufügen einer neuen Karte müssen die neuen Felder initial leer sein ). Möglich sind minimal 1 Karte und maximal 5 Karten.
4. In der **CUSTOMER** Variable gibt es eine Property: "accounts". Die Konten sollen in einer Bootstrap Tabelle angezeigt werden und der Benutzer muss die Möglichkeit haben **NUR EINS** dieser Konten auszuwählen.
   1. Nur die Konten mit currency: "CHF" **UND** type: "moneyAccount" dürfen in der Liste erscheinen
   2. Das Konto mir der id: "account59" hat ein Property selected: true, also muss dieses Konto beim Laden der Seite schon ausgewählt sein. ( Dynamische Lösung und nicht über die genaue ID )
5. Am Ende der Form muss es einen Knopf geben, der das Ergebnis ermittelt. Beim Klicken dieses Knopfes muss in der Konsole das Ergebnis ausgegeben werden.
   1. In dieser Aufgabe wollen wir das Konzept Input / Output nutzen. Dies bedeutet, dass genau die gleiche Struktur, die als Input für die Applikation / Komponente genutzt wird, auch als Output benutzt werden muss ( `src/app/data/model.ts -> CustomerInputOutput`)

## Viel Erfolg!!!
