import { Country, CustomerInputOutput } from "./model";

export const CUSTOMER: CustomerInputOutput = {
    firstName: "Hans",
    lastName: "Mustermann",
    maritalStatus: "Single",
    country: "",
    cards: [
        {
            id: "card576",
            name: "Card One",
            color: "Blue",
            limit: 10000
        }
    ],
    accounts: [
        {
            id: "account54",
            name: "Account one",
            balance: 5000,
            currency: "CHF",
            type: "moneyAccount",
            selected: false
        },
        {
            id: "account676",
            name: "Account two",
            balance: 10000,
            currency: "CHF",
            type: "other_",
            selected: false
        },
        {
            id: "account59",
            name: "Account three",
            balance: 2000,
            currency: "CHF",
            type: "moneyAccount",
            selected: true
        },
        {
            id: "account29",
            name: "Account four",
            balance: 2000,
            currency: "CHF",
            type: "moneyAccount",
            selected: false
        },
        {
            id: "account66",
            name: "Account five",
            balance: 15000,
            currency: "EUR",
            type: "moneyAccount",
            selected: false
        }
    ]
}

export const VALID_COUNTRIES: Country[] = [
    {
        id: "C1",
        name: "Switzerland"
    },
    {
        id: "C2",
        name: "Germany"
    },
    {
        id: "C3",
        name: "England"
    },
    {
        id: "C4",
        name: "Italy"
    },
    {
        id: "C5",
        name: "Spain"
    }
]