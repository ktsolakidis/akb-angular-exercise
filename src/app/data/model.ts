export interface CustomerInputOutput {
    firstName: string;
    lastName: string;
    maritalStatus: string;
    country: string;
    cards: Card[];
    accounts: Account[];
}

export interface Card {
    id: string;
    name: string;
    color: string;
    limit: number;
}

export interface Account {
    id: string;
    name: string;
    balance: number;
    currency: string;
    type: string;
    selected: boolean;
}

export interface Country {
    id: string;
    name: string;
}